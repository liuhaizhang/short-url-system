# 短网址系统

#### 介绍
短网址系统：
1、使用flask+redis+mysql实现
2、在前端方面上没有太多的修饰功能

#### 软件架构
软件架构说明


#### 安装教程

pip install -r req.txt

#### 使用说明

1、系统中要安装好mysql和redis数据库
    1.1、在mysql数据库中，新建数据库short_url
    1.2、执行数据迁移命令
        python app.py db init       #初始化时执行
        python app.py db migrate    #后续有更新表操作时，执行下面这两条命令
        python app.py db upgrade
2、启动项目
python app.py runserver  -h 0.0.0.0 -p 5000

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

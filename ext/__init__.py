from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from flask_restful import Api
from flask_caching import Cache
db = SQLAlchemy() #数据库对象
api = Api() #api
cors = CORS() #跨域
cache = Cache() #缓存
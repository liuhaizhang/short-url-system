数据库迁移目录：
1、初始化操作：只执行一次
python app.py db init

2、更新数据库后操作：
#生成数据库迁移文件
python app.py db migrate

#执行数据库迁移文件
python app.py db upgrade


项目介绍：
1、将长url转成16位的字符串
2、使用到mysql+redis
3、通过hashlib的算法


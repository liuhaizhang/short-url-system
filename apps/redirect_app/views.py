from flask.views import MethodView
from flask import request,redirect,jsonify
from apps.short_url_app import models as short_models
from base.public import REDIRECT_HOST
class RedirectView(MethodView):
    def get(self,url):
        '''
        功能：通过16位字符串，查询数据库，得到长网址，跳转到长网址上
        :param url: 长网址hash得到的16位字符串，通过该字符串，查询数据库，得到长网址，跳转到长网址上
        :return:
        '''
        short_obj = short_models.ShortUrlModel.query.filter_by(
            short_url = url
        ).first()
        if short_obj:
            redirect_url = short_obj.long_url
            return redirect(redirect_url,code=301)
        else:
            return jsonify({'code':400,'error':'查询不到该短地址'})
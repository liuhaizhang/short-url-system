from flask import Blueprint
from . import views

redirect_bp = Blueprint('redirect',__name__,url_prefix='')

redirect_bp.add_url_rule('/<string:url>',view_func=views.RedirectView.as_view('redirect-app-first'))
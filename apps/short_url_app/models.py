from ext import db
class ShortUrlModel(db.Model):
    __tablename__='short_url_table'
    id = db.Column(db.Integer,primary_key=True)
    short_url = db.Column(db.String(32),comment='短url',index=True)
    long_url = db.Column(db.String(256),comment='原url')
    the_time = db.Column(db.String(32),comment='时间戳')
from flask import Blueprint
from . import views

user_bp = Blueprint('short_url',__name__,url_prefix='/short')

user_bp.add_url_rule('/html',view_func=views.HomeView.as_view('short-html'))  #// 前端html页面，提交长地址转短地址
user_bp.add_url_rule('/url/make',view_func=views.MakeShortUrlView.as_view('short-url-make')) #post将长地址转短地址，get 通过短地址获取长地址
user_bp.add_url_rule('/url/max-id',view_func=views.ShortUrlMaxIdView.as_view('short-url-max-id')) #查看当前总的地址量

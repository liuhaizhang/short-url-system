from flask import jsonify,request
from flask_migrate import Migrate,MigrateCommand
from flask_script import Manager,Command
from apps import create_app
from ext import db

#模型类
from apps.short_url_app.models import *

app = create_app()

#应用于数据库用migrate管理
migrate = Migrate(app,db)
manager = Manager(app=app)
manager.add_command('db',MigrateCommand)
#启动命令：python app.py runserver -h 0.0.0.0 -p 8000

if __name__ == '__main__':
    manager.run()
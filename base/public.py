import hashlib
import string
import time
import re
import random

#记录当前最新创建的短网址的id，存在redis数据库中的key
URL_MAX_ID_URL = 'url_max_id'
#生成的字符串拼接下面的域名生成新的地址,访问拼接后的地址进行跳转,[后面改为部署的域名]
REDIRECT_HOST = 'http://127.0.0.1:5000/'

#判断是否是http网址
def is_http_url(url):
    pattern = re.compile(r'^https?://[^\s]+$')
    if re.match(pattern, url):
        return True
    else:
        return False

#生成随机数,为了进一步避免生成重复字符串，加上随机数
def make_random_code():
    characters = string.ascii_letters + string.digits+'@#!$%^&*()_+-=}{[]:;"?/.>,<|\\`~'  # 字符集，大小写字母+数字+特殊字符
    #3个0-1的随机浮点数
    lis = []
    for i in range(10):
        lis.append(str(random.random()))
        lis.append(random.choice(characters))
        lis.append(str(random.randint(1,10000)))
    ret_str = ''.join(random.choice(lis) for _ in range(50))
    return ret_str

#将长网址转成字符串的的算法
def shorten_url_sha256(long_url,url_id,the_time:str):
    '''
    :param url_id: 该短地址要存到数据库后的id （由redis存一个自增的数值，代表id）
    :param long_url: 正常的网址地址+‘####数据库自增id+时间戳’   避免生成重复短网址的问题
    :param the_time: 时间戳
    :return: 字符串
    '''

    operate_url = f'{long_url}####[{url_id}-{the_time}-{make_random_code()}]'
    characters = string.ascii_letters + string.digits  # 字符集，共 62 个字符
    #string.ascii_letters 大小写字母字符串，52位，string.digits=0-9字符串，10位字符

    hash_value = hashlib.sha256(operate_url.encode()).hexdigest()  # 使用 sha256 哈希函数生成哈希值，64 位字符串
    print(hash_value)
    sub_strings = [hash_value[i:i+4] for i in range(0, 64, 4)]  # 划分子字符串
    print(sub_strings)
    decimal_nums = [int(sub_string, 16) for sub_string in sub_strings]  # 每个子字符串转换成十进制数
    print(decimal_nums)
    short_url = ''
    for num in decimal_nums:
        short_url += characters[num%62]  # 映射到字符集
    return short_url

if __name__ == '__main__':
    print(is_http_url('http://abc.theay.com/api/sdfs'))
    print(make_random_code())
    print(shorten_url_sha256('jjjsdfs',23,'sdfssgjhgfsg'))

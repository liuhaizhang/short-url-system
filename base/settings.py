import os
def get_database(dic):
    '"mysql+pymysql://root:123456@localhost:3306/short_url?charset=utf8"'
    engine = dic.get('ENGINE')
    driver = dic.get('DRIVER')
    user = dic.get('USER')
    host = dic.get('HOST')
    password = dic.get('PASSWORD')
    port = dic.get("PORT")
    name = dic.get('NAME')
    dbinfo = f"{engine}+{driver}://{user}:{password}@{host}:{port}/{name}?charset=utf8"
    return dbinfo

#全局通用配置类
class Config:
    """项目配置核心类"""
    DEBUG=True
    LOG_LEVEL = "INFO"
    SECRET_KEY= '8hdj^sasdas6736475#$#5&GHG'
    BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    # TEMPLATE_FOLDER = '../templates'  #配置了html存放的目录，使用相对路径 无法生效，在创建app时设置
    # STATIC_FOLDER = '../static' #配置静态目录，使用相对路径    无法生效，在创建app时设置
    # 中文乱码
    JSON_AS_ASCII = False
    # # 配置redis
    # # 项目上线以后，这个地址就会被替换成真实IP地址，mysql也是
    # REDIS_HOST = '127.0.0.1'
    # REDIS_PORT = 6379
    # REDIS_MAX_CONNECTIONS = 1000 #设置同时可以被使用的最大 Redis 连接数
    # # REDIS_PASSWORD = 'your password'
    # REDIS_POLL = 500  #连接池
    ##数据库配置
    dbinfo={
        'ENGINE':'mysql',
        'DRIVER':'pymysql',
        'USER':'root',
        'PASSWORD':'123456',
        'HOST':"127.0.0.1",
        "PORT":"3306",
        'NAME':'short_url'
    }
    # 数据库连接格式
    SQLALCHEMY_DATABASE_URI = get_database(dbinfo)
    # 动态追踪修改设置，如未设置只会提示警告
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    # 查询时会显示原始SQL语句
    SQLALCHEMY_ECHO = False
    # 数据库连接池的大小
    SQLALCHEMY_POOL_SIZE=1000
    #指定数据库连接池的超时时间
    SQLALCHEMY_POOL_TIMEOUT=30
    # 控制在连接池达到最大值后可以创建的连接数。当这些额外的 连接回收到连接池后将会被断开和抛弃。
    SQLALCHEMY_MAX_OVERFLOW=2

    #配置日志时，需要设置False，只能flask才能捕获移除写到日志文件中
    PROPAGATE_EXCEPTIONS = False


class DevConfig(Config):
    DEBUG = True

class TestConfig(Config):
    DEBUG = True

class Online(Config):
    DEBUG = False

envs = {
    'dev':DevConfig,
    'test':TestConfig,
    'online':Online,
    'default':Config
}
